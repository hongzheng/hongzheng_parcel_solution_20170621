﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelPackage
{
    public enum PackageType
    {
        SmallPackage = 0,
        MediumPackage = 1,
        LargePackage = 2,
        OverSizePackage =3,
        OverWeightPackage = 4
    }
}
