﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelPackage
{
    public class Parcel
    {
        public const decimal SmallPackagePrice = 5;
        public const decimal MediumPackagePrice = 7.5M;
        public const decimal LargePackagePrice = 8.5M;

        public Parcel(decimal length, decimal breadth, decimal height, decimal weight) 
        {
            Length = length;
            Breadth = breadth;
            Height = height;
            Weight = weight;
            CalculateTypeOfParcel();
        }

        private decimal Length;

        private decimal Breadth;

        private decimal Height;

        private decimal Weight;

        public PackageType TypeOfParcel;

        private void CalculateTypeOfParcel()
        {
            if(Weight > 25)
            {
                TypeOfParcel = PackageType.OverWeightPackage;
            }
            else
            {
                if (Length <= 200 && Breadth <= 300 && Height <= 150)
                {
                    TypeOfParcel = PackageType.SmallPackage;
                }
                else if (Length <= 300 && Breadth <= 400 && Height <= 200)
                {
                    TypeOfParcel = PackageType.MediumPackage;
                }
                else if (Length <= 400 && Breadth <= 600 && Height <= 250)
                {
                    TypeOfParcel = PackageType.LargePackage;
                }
                else
                {
                    TypeOfParcel = PackageType.OverSizePackage;
                }
            }
        }
    }
}
