﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParcelPackage
{
    public class ParcelPacker : IParcelPacker
    {
        private List<Parcel> _parcels;

        public ParcelPacker(List<Parcel> parcels)
        {
            _parcels = parcels;
        }

        public decimal CalculateTotalCosts()
        {
            decimal total = 0;

            foreach (var p in _parcels)
            {
                switch (p.TypeOfParcel)
                {
                    case PackageType.SmallPackage:
                        total += Parcel.SmallPackagePrice;
                        break;
                    case PackageType.MediumPackage:
                        total += Parcel.MediumPackagePrice;
                        break;
                    case PackageType.LargePackage:
                        total += Parcel.LargePackagePrice;
                        break;         
                    default:
                        throw new Exception("invalid parcel");
                }
            }
            return total;
        }
    }
}
