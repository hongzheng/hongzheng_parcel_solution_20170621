﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParcelPackage;

namespace ParcelConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string Response = "not finished yet";
            List<Parcel> parcels = new List<Parcel>();
            Console.WriteLine("Please enter 'done' to quit and get the total cost");
            while (Response.ToLower() != "done")
            {
                var parcel = GetOneParcelFromUser();
                if (parcel.TypeOfParcel == PackageType.OverSizePackage)
                {
                    Console.WriteLine("Oversize parcel entered: max length is 400 mm, max Breadth 600mm and max Height 250 mm");
                }
                else if (parcel.TypeOfParcel == PackageType.OverWeightPackage)
                {
                    Console.WriteLine("Oversize parcel entered: max weight is 25 kg");
                }
                else
                {
                    parcels.Add(parcel);
                }

                Console.WriteLine("type 'done' to get total cost or any key then enter to enter next parcel.");
                Response = Console.ReadLine();
            }

            ParcelPacker packer = new ParcelPacker(parcels);
            Console.WriteLine("Your total cost for this post is ${0}", packer.CalculateTotalCosts().ToString());
            Console.ReadLine();
        }

        private static Parcel GetOneParcelFromUser()
        {
            Console.WriteLine("Please enter parcel length in milli metre:");
            decimal length = decimal.MinValue;
            while (length == decimal.MinValue)
            {
                string strLength = Console.ReadLine();
                decimal.TryParse(strLength, out length);
                if(length == decimal.MinValue)
                    Console.WriteLine("Please enter parcel length in milli metre by decimal format: e.g. 300.50");
            }

            Console.WriteLine("Please enter parcel breadth in milli metre:");
            decimal breadth = decimal.MinValue;
            while (breadth == decimal.MinValue)
            {
                string strBreath = Console.ReadLine();
                decimal.TryParse(strBreath, out breadth);
                if (breadth == decimal.MinValue)
                    Console.WriteLine("Please enter parcel breadth in milli metre by decimal format: e.g. 400.50");
            }

            Console.WriteLine("Please enter parcel height in milli metre:");
            decimal height = decimal.MinValue;
            while (height == decimal.MinValue)
            {
                string strHeight = Console.ReadLine();
                decimal.TryParse(strHeight, out height);
                if (height == decimal.MinValue)
                    Console.WriteLine("Please enter parcel height in milli metre by decimal format: e.g. 400.50");
            }

            Console.WriteLine("Please enter parcel weight in Kg:");
            decimal weight = decimal.MinValue;
            while (weight == decimal.MinValue)
            {
                string strWeight = Console.ReadLine();
                decimal.TryParse(strWeight, out weight);
                if (height == decimal.MinValue)
                    Console.WriteLine("Please enter parcel weight in Kg by decimal format: e.g. 20.50");
            }

            return new Parcel(length, breadth, height, weight);
        }
    }
}
